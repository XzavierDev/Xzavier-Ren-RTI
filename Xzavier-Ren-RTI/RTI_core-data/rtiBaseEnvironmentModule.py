#Initialise Base Environment

import linecache
import transientStorageModule as ts
import crashHandlerModule as ch
import os
import sys

dirpath = linecache.getline("rootDirectory.cfg", 1).rstrip("\n")
sys.path.append(""+dirpath+"")
os.chdir(""+dirpath+"")

user = ts.usernamec
import rtiShellModule
rtiShellModule.shell()

