import time
import console

def exitNow():
	print("This version of RTI does not yet support logging.")
	time.sleep(3)
	exit()
	
def preEnv():
	console.clear()
	print(""":( Something went wrong.
	
	Ren RTI has encountered a serious error and needs to terminate.
	This may be because you are not running an up to date version 
	of Pythonista, you modified critical application files
	or there is a bug in the application. Please try to
	update your software before continuing.""")
	print("Press ENTER to exit.")
	cont = input ("")
	exitNow()
