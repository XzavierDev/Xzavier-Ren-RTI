#!/usr/bin/env python3
#-*-coding: utf-8-*-
SYS_NAME = "dashboardModule"
SYS_VERSION = "0923"
CRITICAL = True
MODPACK = "stock"
TESTED_ON = ["0921","0923"]
COMPATIBLE_PLATFORMS = ["NIX"]
MIN_PY_VERSION = "360"
#End of mandatory variables

import os
import linecache
import sys
import time

workingdir = linecache.getline("dirinfo3.cfg", 1).rstrip('\n')

version = ""


sys.path.append(""+workingdir+"/ramdisk")
import transientStorageModule as alwaysonrd


version = linecache.getline(""+workingdir+"/buildprop.xcf", 3)
print ("""
Welcome to Xzavier Ren RTI v"""+version+""".
Copyright 2018 Rylan Kivolski - free to use
modify and distribute.

Notifcations [0]
