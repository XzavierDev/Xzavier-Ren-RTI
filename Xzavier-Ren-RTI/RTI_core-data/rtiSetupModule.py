import console
import os
import sys

print("Welcome to setup! This module will configure your install.\n")
print("Please be aware that this edition is only for iOS devices.")
cont = input("Press ENTER to continue.")

print("Please type a username below.")
username = input("")

while True:
	console.clear()
	print("Select your device:")
	print("""
	1. iPhone 5, iPad 4, iPad Mini 1, iPod 5 or earlier.
	2. iPhone 5S/SE. iPod Touch 6.
	3. iPhone 6/6S/7/8.
	4. iPhone 6+/7+/8+.
	5. iPhone X/XS.
	6. iPhone XR.
	7. iPhone XS Max.
	
	8. iPad Mini 2 or later.
	9. iPad Air (9.7") or later.
	10. iPad Pro 10.5".
	11. iPad Pro 12.9". """)
	devicestr = input("")
	try:
		device = (int(devicestr))
		if device > 11 or device <= 0:
			print("Choose a valid number.")
			time.sleep(4)
			continue
	except TypeError:
		print("Selection not valid. enter a number.")
		time.sleep(4)
		continue
	break

f = open("rtiSettings.cfg", "w")
f.write(""+username+"\n"+devicestr+"")
f.close()

print("\nSetup is complete, relaunch to continue.")

