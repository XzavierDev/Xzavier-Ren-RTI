def hardwareFeatureTable(deviceOrQuery):
	features = ["_NONE_"]
	
	has3dTouch = ["iPhone X","iPhone 8 Plus","iPhone 8","iPhone 7 Plus","iPhone 7","iPhone 6S Plus", "iPhone 6S"]
	
	has47CompatibilityMode = ["iPhone X","_iPad ALL_"]
	
	has55CompatibilityMode = ["_iPad ALL_"]
	
	hasTelephotoLens = ["iPhone X","iPhone 8 Plus","iPhone 7 Plus"]
	
	hasFrontPortraitMode = ["iPhone X"]
	
	hasApplePencilSupport = ["_iPad:Pro ALL_","iPad 9.7 2018"]
	
	hasSmartConnector = ["_iPad:Pro ALL_"]
	
	hasWirelessCharging = ["iPhone X","iPhone 8 Plus","iPhone 8"]
	
	hasHeySiriUntethered = ["iPhone X","iPhone 8 Plus","iPhone 8","iPhone 7 Plus","iPhone 7","iPhone 6S Plus", "iPhone 6S","iPhone SE","iPad Pro 12.9 2017","iPad Pro 10.5 2017","iPad Pro 9.7"]
	
	hasFullMultiTasking = ["_iPad:Pro ALL_","iPad Air 2"]
	
	hasTrueTone = ["iPhone X","iPhone 8 Plus","iPhone 8", "iPad Pro 12.9 2017","iPad Pro 9.7"]
	
	hasOLED = ["iPhone X"]
	
	
	is58Inches = ["iPhone X"]
	is55Inches = ["iPhone 8 Plus","iPhone 7 Plus","iPhone 6S Plus","iPhone 6 Plus"]
	is47Inches = ["iPhone 8","iPhone 7","iPhone 6S","iPhone 6"]
	is40Inches = ["iPhone SE", "iPhone 5S"]
	
	is97Inches = ["iPad Pro 9.7","iPad 9.7 2018","iPad 9.7 2017","iPad Air 2","iPad Air"]
	is105Inches = ["iPad Pro 10.5 2017"]
	is129Inches = ["iPad Pro 12.9 2017","iPad Pro 2015"]
	
	is79Inches = ["iPad Mini 4","iPad Mini 3","iPad Mini 2"]
	
	moderniPhone = ["iPhone X"]
	moderniPad = []
	
	#define identifing section
	
	if "Q:" in deviceOrQuery:
		query = True
	else:
		query = False
	
