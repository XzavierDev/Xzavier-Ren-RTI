import os
import console
import linecache
import sys
import time

dirpath  = os.path.dirname(os.path.realpath(__file__))
if linecache.getline(""+dirpath+"/rtiSettings.cfg", 1) != "":
	import rtiCoreModule
else:
	import rtiSetupModule
