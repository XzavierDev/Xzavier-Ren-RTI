import time
import os
import console
import sys

currentdirectory = os.path.dirname(os.path.realpath(__file__))

print("Welcome to Ren RTI, © 2018 Rylan Kivolski. Free to distribute.")
time.sleep(3)
console.clear()

sys.path.append(""+currentdirectory+"/RTI_core-data")
currentdirectory = currentdirectory+"/RTI_core-data"
os.chdir(""+currentdirectory+"")
f = open("rootDirectory.cfg", "w")
f.write(currentdirectory)
f.close()
import chainlink
